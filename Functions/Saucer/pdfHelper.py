import img2pdf
import requests
import os
import random
from PIL import Image


class PdfHelper:
    def __init__(self, session, name):
        self.username = session.name
        self.urlList = session.imageUrl
        self.name = name
        self.qual = session.quality
        self.path = "export/" + self.username + "/"
        if not os.path.exists(self.path):
            os.mkdir(self.path)

    def getImg(self):
        index = 0
        self.imgPath = []
        for url in self.urlList:
            # Download image
            f = open(self.path + str(index) + ".jpg", "wb")
            f.write(requests.get(url).content)
            f.close()

            Image.open(self.path + str(index) + ".jpg").convert(
                "RGB").save(self.path + str(index) + ".jpg", quality=self.qual)
            self.imgPath.append(self.path + str(index) + ".jpg")
            index += 1

    def convert(self):
        # convert and save
        self.name += ".pdf"
        with open(self.path + self.name, "wb") as f:
            f.write(img2pdf.convert(self.imgPath))

    def checkName(self):
        while(os.path.exists(self.path + self.name)):
            self.name += str(random.randint(0, 9))

    def cleanup(self):
        for root, dirs, files in os.walk(self.path, topdown=False):
            for name in files:
                os.remove(os.path.join(root, name))
            for name in dirs:
                os.rmdir(os.path.join(root, name))
