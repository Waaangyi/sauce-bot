import discord
import os
from Functions.Saucer.sauce import Sauce
from Functions.Saucer.pdfHelper import PdfHelper
from datetime import datetime
from discord.ext import commands


class Saucer(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.sessions = {}

    @commands.command(name="start")
    async def startSaucer(self, ctx):
        name = ctx.message.author.name
        if name in self.sessions:
            await ctx.send("You already started a session, use saucer save or saucer discard to end session!")
            return

        self.sessions[name] = Sauce(name, ctx.message.created_at)
        await ctx.send("Session established! Use saucer save or saucer discard to end session!")

    @commands.command(name="save")
    async def saveSaucer(self, ctx):
        name = ctx.message.author.name
        if name not in self.sessions:
            await ctx.send("You did not have a open session, use saucer start to start session!")
            return

        # parse command
        cmd = ctx.message.content.split()
        if "send" not in cmd and "download" not in cmd:
            await ctx.send("Unknown instruction")
            return
        try:
            cmd.pop(0)
            cmd.pop(0)

            channelName = "direct message"
            if "send" in cmd:
                channelName = cmd[-1]
                cmd.pop(-1)
            cmd.pop(-1)

            filename = "_".join(cmd)
            await ctx.send(filename + " to " + channelName)

        except Exception as e:
            await ctx.send("Something went wrong")
            await ctx.send(str(e))
            return

        # grab the messages
        # 200 seems to be a reasonable limit
        self.sessions[name].cleanUp()
        historyLimit = 200
        channel = ctx.message.channel
        counter = 0
        async with channel.typing():
            async for msg in channel.history(limit=historyLimit):
                if msg.author.name == name and len(msg.attachments) != 0 and msg.created_at.timestamp() >= self.sessions[name].startTime.timestamp():
                    # Grab the attachment(s)
                    tempurl = []
                    for at in msg.attachments:
                        tempurl.append(at.url)
                        counter += 1

                    tempurl.reverse()
                    self.sessions[name].imageUrl.extend(tempurl)
                    # React to the message
                    await msg.add_reaction("💾")
            if counter == 0:
                await ctx.send("It seems like there is nothing to grab, send attachments or try again")
                return
            await ctx.send("I found " + str(counter) + " attachments, please wait while I try to convert them")
            # reverse
            self.sessions[name].imageUrl.reverse()

            # Convert to pdf
            filepath = ""
            try:
                p = PdfHelper(self.sessions[name], filename)
                p.getImg()
                p.checkName()
                p.convert()
                filepath = p.path + p.name
                filename = p.name
            except Exception as e:
                await ctx.send("Something went wrong while trying to convert the messages (bug or network error?)\nYour session is still saved")
                await ctx.send("Error message is: " + str(e))
                return

            try:
                if channelName == "direct message":
                    await self.directMessage(ctx, filepath, filename)
                else:
                    channelList = []
                    for channel in ctx.guild.text_channels:
                        channelList.append(channel.name)
                    if channelName not in channelList:
                        await ctx.send("Channel name not found, attempt to direct message")
                        await self.directMessage(ctx, filepath, filename)
                    else:
                        for channel in ctx.guild.text_channels:
                            if(channel.name == channelName):
                                with open(filepath, "rb") as f:
                                    await channel.send(file=discord.File(f, filename))

                self.sessions.pop(name)
            except Exception as e:
                await ctx.send("Something went wrong while trying to send the converted pdf")
                await ctx.send("Error message is: " + str(e))
                await ctx.send("If the payload is too large, use -saucer quality to change the quality and try again")
        p.cleanup()

    @commands.command("discard")
    async def discardSaucer(self, ctx):
        name = ctx.message.author.name
        self.sessions.pop(name)

    @commands.command("sessions")
    async def listSaucer(self, ctx):
        try:
            embed = discord.Embed(title="All active sessions")
            for key in self.sessions:
                embed.add_field(name=self.sessions[key].name, value="Started " +
                                str(ctx.message.created_at - self.sessions[key].startTime) + " ago")
            await ctx.send(embed=embed)
        except Exception as e:
            await ctx.send(str(e))

    @commands.command(name="clear")
    @commands.has_permissions(administrator=True)
    async def clearSaucer(self, ctx):
        self.sessions = {}
        await ctx.message.add_reaction("🆗")

    async def directMessage(self, ctx, filepath, filename):
        try:
            with open(filepath, "rb") as f:
                await ctx.author.send(file=discord.File(f, filename))
        except Exception as e:
            await ctx.send("Error: " + str(e))
            with open(filepath, "rb") as f:
                await ctx.channel.send(file=discord.File(f, filename))

    @commands.command(name="quality")
    async def changeCompression(self, ctx):
        msg = ctx.message.content.split()
        name = ctx.message.author.name
        try:
            if(msg[-1] == "quality"):
                await ctx.send(self.sessions[name].quality)
            else:
                self.sessions[name].quality = int(msg[-1])
        except Exception as e:
            await ctx.send(str(e))


def setup(bot):
    bot.add_cog(Saucer(bot))
